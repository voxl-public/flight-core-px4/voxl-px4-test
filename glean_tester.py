import os
import logging
import subprocess

from pyulog import ULog
from datetime import datetime

def check_uorb_status(topic):
    p = subprocess.run(['adb', 'shell', 'px4-listener', str(topic)], capture_output=True, text=True)
    return p.stdout

def pull_most_recent():
    p = subprocess.run(['adb', 'shell', 'find', '/data/px4/log', '-type', 'f', '-name', "'*.ulg'", '-exec', 'stat', "-c'%Y", "%n'", '{}', '+', '|', 'sort', '-n', '|', 'tail', '-n', '1', '|', 'cut', "-d'", "'", '-f', '2'], capture_output=True, text=True)
    ret_file = subprocess.run(['adb', 'pull', p.stdout.strip('\n'), 'ulogs'])
    return p.stdout.strip('\n')

def glean_ulog(ulog):
    
    ulog_file = ULog(ulog)
    mission_result_data = True if 1 in ulog_file.get_dataset('mission_result').data['finished'] else False
    assert mission_result_data, 'Mission Result from ulog shows mission was never completed'

    positional_data = ulog_file.get_dataset('vehicle_local_position')

    x_positions = positional_data.data['x']
    y_positions = positional_data.data['y']
    z_positions = positional_data.data['z']

    zipped_positions = list(zip(x_positions,y_positions,z_positions))
    median_positions = zipped_positions[int(len(zipped_positions)/2)]
    
    summed_initial_pos = sum(zipped_positions[0])
    summed_median_pos = sum(median_positions)

    assert summed_initial_pos + 20 < summed_median_pos, 'Error in local positional setpoints, drone did not travel distance expected from mission'
    
    return True

if __name__=='__main__':
    output_status = check_uorb_status("vehicle_status")
    output_accel = check_uorb_status("sensor_accel")
    output_gyro = check_uorb_status("sensor_gyro")
                                    
    assert "timestamp" in output_status and "hil_state" in output_status, str(output_status)
    assert "timestamp" in output_status and "device_id" in output_accel, str(output_accel)
    assert "timestamp" in output_status and "device_id" in output_gyro, str(output_gyro)

    ulog_retval = pull_most_recent()
    glean_retval = glean_ulog(('ulogs/' + ulog_retval.split('/')[-1]))

    print("Completed checks - passed asserts")

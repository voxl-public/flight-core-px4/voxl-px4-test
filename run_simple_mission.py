#!/usr/bin/env python3

import asyncio

from mavsdk import System
import mavsdk.mission
import mavsdk.mission_raw
import mavsdk.telemetry

async def run():
    drone = System()

    print("Waiting for drone to connect...")
    await drone.connect(system_address="udp://:14551")
    
    async for state in drone.core.connection_state():
        if state.is_connected:
            print("Connected to drone!")
            break

    async for landed in drone.telemetry.landed_state():
        print(f"Landed state: {landed}")
        break

    print("Clearing old mission")
    await drone.mission.clear_mission()
        
    print("Importing a new mission from QGC file")
    mission_import_data = await drone.mission_raw.import_qgroundcontrol_mission("flight-plans/ModalAI-HITL-Test.plan")
    print(f"{len(mission_import_data.mission_items)} mission items imported")
    
    print("Uploading the new mission")
    await drone.mission_raw.upload_mission(mission_import_data.mission_items)
    print("New mission uploaded")

    print("Waiting for drone to have a global position estimate")
    async for health in drone.telemetry.health():
        if health.is_global_position_ok and health.is_home_position_ok:
            print("Global position estimate OK")
            break
        
    print("-- Arming")
    await drone.action.arm()
    
    print("-- Starting mission")
    await drone.mission_raw.start_mission()

    async for progress in drone.mission_raw.mission_progress():
        if progress.current == progress.total:
            print("-- Mission completed")
            break
        else:
            print(f"-- On mission item {progress.current} out of {progress.total}")

    async for landed in drone.telemetry.landed_state():
        if mavsdk.telemetry.LandedState(landed) == landed.LANDING:
            print("-- Drone is landing")
        elif mavsdk.telemetry.LandedState(landed) == landed.ON_GROUND:
            print("-- Drone has landed")
            break

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())

#!/usr/bin/env python3

import asyncio

from mavsdk import System
import mavsdk.mission_raw


async def run():
    drone = System()
    await drone.connect(system_address="udp://:14551")
    
    print("Waiting for drone to connect...")
    async for state in drone.core.connection_state():
        if state.is_connected:
            print(f"-- Connected to drone!")
            break
        
    mission_import_data = await drone.mission_raw.import_qgroundcontrol_mission("flight-plans/ModalAI-HITL-Test.plan")
    print(f"{len(mission_import_data.mission_items)} mission items imported")
    
    await drone.mission_raw.upload_mission(mission_import_data.mission_items)
    print("Mission uploaded")

    print("Waiting for drone to have a global position estimate...")
    async for health in drone.telemetry.health():
        if health.is_global_position_ok and health.is_home_position_ok:
            print("-- Global position estimate OK")
            break
        
    print("-- Arming")
    await drone.action.arm()
    
    print("-- Starting mission")
    await drone.mission_raw.start_mission()

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())
